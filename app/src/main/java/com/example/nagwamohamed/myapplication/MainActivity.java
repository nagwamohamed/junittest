package com.example.nagwamohamed.myapplication;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import org.junit.Assert;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Button add =(Button)findViewById(R.id.button);
        final TextView text=(TextView)findViewById(R.id.textView);
        final EditText text1=(EditText)findViewById(R.id.editText2);
        final EditText text2=(EditText)findViewById(R.id.editText);


        add.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                Calculater calculator=new Calculater();
                int number1= Integer.parseInt(String.valueOf(text1.getText()));

                int number2=Integer.parseInt(String.valueOf(text2.getText()));



                int result = calculator.sum(number1,number2);
         text.setText(Integer.toString(result));


            }
        });
    }

}

