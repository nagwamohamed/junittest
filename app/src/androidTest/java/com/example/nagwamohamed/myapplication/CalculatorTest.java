package com.example.nagwamohamed.myapplication;

import org.junit.After;
import org.junit.Assert;
import org.junit.Before;

import  com.example.nagwamohamed.myapplication.Calculater;
import static org.junit.Assert.*;

/**
 * Created by nagwa.mohamed on 10/10/2017.
 */
public class CalculatorTest {
    Calculater calculator;
    @Before
    public void setUp() throws Exception {
        calculator = new Calculater();
    }

    @After
    public void tearDown() throws Exception {

    }

    @org.junit.Test
    public void sum() throws Exception {

        int result = calculator.sum(2,2);


        Assert.assertEquals(4,result);


    }

}